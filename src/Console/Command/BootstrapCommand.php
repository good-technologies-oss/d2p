<?php

declare(strict_types=1);

namespace GoodTechnologies\Oss\D2p\Console\Command;

use GoodTechnologies\Oss\D2p\Enum\OperatingSystem;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use UnhandledMatchError;

use function array_keys;
use function exec;
use function system;

final class BootstrapCommand extends Command
{
    public function __construct()
    {
        parent::__construct('bootstrap');
    }

    protected function configure()
    {
        $this
            ->setDescription('Bootstraps the D2P tool, installing dependencies, etc.');
    }


    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $io = new SymfonyStyle($input, $output);

        $dependencies = [
            'Python3' => [$this, 'installPython'],
            'Ansible' => [$this, 'installAnsible'],
            'Terraform' => [$this, 'installTerraform'],
        ];

        $io->text('This script will install the following dependencies on your local machine…');
        $io->listing(array_keys($dependencies));

        $os = $this->detectOs();

        if (null === $os) {
            return self::FAILURE;
        }

        $io->info("Detected \"{$os->name}\" operating system.");

        foreach ($dependencies as $name => $callback) {
            $io->title("Installing {$name}");

            $success = $callback($io, $os);

            if (!$success) {
                return self::FAILURE;
            }
        }

        $io->success('Done!');

        return self::SUCCESS;
    }

    private function detectOs(): ?OperatingSystem
    {
        exec('uname', $output, $exitCode);

        try {
            return match ($output[0] ?? null) {
                'Darwin' => OperatingSystem::Darwin,
            };
        } catch (UnhandledMatchError $e) {
            $io->error("Your OS ({$output[0]}) is not currently supported.");

            return null;
        }
    }

    private function installAnsible(SymfonyStyle $io, OperatingSystem $os): bool
    {
        # If already installed, skip install…
        if ($this->doesCommandExists('ansible')) {
            $io->info('Ansible is already installed. Skipping.');

            return true;
        }

        system('python3 -m pip install --user ansible', $exitCode);

        if (0 === $exitCode) {
            return true;
        }

        $io->error('Install failed!');

        return false;
    }

    private function doesCommandExists(string $command): bool
    {
        exec('command -v ' . $command . ' > /dev/null', $output, $exitCode);

        return 0 === $exitCode;
    }

    private function installPython(SymfonyStyle $io, OperatingSystem $os): bool
    {
        if ($this->doesCommandExists('python3')) {
            $io->info('Python3 is already installed. Skipping.');

            return true;
        }

        system('brew install python', $exitCode);

        if (0 === $exitCode) {
            return true;
        }

        $io->error('Install failed!');

        return false;
    }


    private function installTerraform(SymfonyStyle $io, OperatingSystem $os): bool
    {
        if ($this->doesCommandExists('terraform')) {
            $io->info('Terraform is already installed. Skipping.');

            return true;
        }

        try {
            $installScript = match ($os) {
                OperatingSystem::Darwin => 'brew tap hashicorp/tap &&  brew install terraform',
                OperatingSystem::Linux => <<<SCRIPT
                    set -x
                    
                    sudo apt-get update && sudo apt-get install -y gnupg software-properties-common;
                    
                    wget -O- https://apt.releases.hashicorp.com/gpg | \
                        gpg --dearmor | \
                        sudo tee /usr/share/keyrings/hashicorp-archive-keyring.gpg
                    
                    gpg --no-default-keyring \
                        --keyring /usr/share/keyrings/hashicorp-archive-keyring.gpg \
                        --fingerprint
                    
                    echo "deb [signed-by=/usr/share/keyrings/hashicorp-archive-keyring.gpg] \
                        https://apt.releases.hashicorp.com $(lsb_release -cs) main" | \
                        sudo tee /etc/apt/sources.list.d/hashicorp.list
                    
                    sudo apt update
                    
                    sudo apt-get install terraform
                    SCRIPT,
            };
        } catch (UnhandledMatchError $e) {
            $io->error('Unable to install Python as install steps not know for current host.');

            return false;
        }

        system($installScript, $exitCode);

        if (0 === $exitCode) {
            return true;
        }

        $io->error('Install failed!');

        return false;
    }
}