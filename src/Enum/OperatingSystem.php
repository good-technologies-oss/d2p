<?php

declare(strict_types=1);

namespace GoodTechnologies\Oss\D2p\Enum;

enum OperatingSystem
{
    case Darwin; # MacOS
    case Linux;
}